var Main = function(){};

var SH = function(){
	var _this = this;
	$.when(this.init()).done(function(){
		Main();
	});
};

SH.UA = "";//ユーザーエージェント情報を保持
SH.inputNameArr = [];//nameを格納する配列
SH.requireLinkNum = $(".require-link").length;//必須押下リンクの数
SH.requireLinkCheckedNum = 0;//必須押下リンクの押下済み数
if ($("#downloadFlg").val() === "1" || $("#downloadFlg").val() === "2"){
	SH.requireLinkCheckedNum = SH.requireLinkNum;
}

SH.prototype = {
	init:function(){
    var _this = this;
    var css = "position:fixed;top:4px;left:50%;margin-left:-60px;text-align:center;border-radius:4px;opacity:0.8;z-index:9999;";
    $("head").append("<style>#samplebox{" + css + "} #samplebox.clicked{display:none}</style>");
    $("body").append('<div id="samplebox" class="pd05 fs20 fwb w120 h45 btn--yellow">Sample</div>');
    $("#samplebox").click(function(){
      $("#samplebox").addClass("clicked");
      setTimeout(function(){
      $("#samplebox").removeClass("clicked");
      }, 180000);
    });
		return $.Deferred(function(defer){

			//ユーザーエージェント情報取得
			SH.UA = SH.UTIL.checkUA();

			//ブラウザ名をクラスとして追加
			$("body").addClass(SH.UA);

			//ヘッダーナビをアクティブ化
			SH.UTIL.headerNavActive();

			//ヘッダー固定処理　初期実行
			SH.UTIL.headerFix();

			//ヘッダー固定のイベント追加
			$(window).on("scroll",function(e){
				SH.UTIL.headerFix();
			});

			//トップへ戻るボタン位置固定処理　初期実行
			SH.UTIL.toTopShowHide();

			//トップへ戻るボタン位置固定処理 スクロール、リサイズイベント追加
			$(window).on("scroll resize",function(e){
				SH.UTIL.toTopShowHide();
			});

			//トップへ戻る処理
			$("#to-top,#gologinForm,#gologin").on("click",function(e){
				SH.UTIL.toTop();
			});

			//非活性ボタンEnterキー押下無効化処理
			$(document).on("keydown",".btn-disabled",function(e){
				return !((e.which && e.which == 13)||(e.keycode && e.keycode == 13));
			});

			//ズーム関連初期化
			SH.UTIL.zoomInt();

			//セレクト要素の初期化
			SH.UTIL.selectInt();

			//トグルボタンの初期化
			SH.UTIL.toggleInt();

			//スクロールチェックボックス初期化
			SH.UTIL.scrollCheckboxInt();

			//アコーディオン初期化
			SH.UTIL.accordionInt();

			//お問合せ固定ボタン
			$(".btn__inquiry-fix").on("click",function(e){
				SH.UTIL.inquryShowHide();
			});

			//ボタンパネル内リンク処理
			SH.UTIL.btnPanelLink();

			//必須入力処理初期化
			SH.UTIL.requireInputInt();

			//datePicker初期化
			SH.UTIL.datePickerInt();

			//ウィンドウを閉じる
			SH.UTIL.windowClose();

			//入力内容をクリア
			SH.UTIL.clearFormInt();


			//ログインエリア開閉
			SH.UTIL.loginAreaOpenClose();

			//フォントサイズ調整
			$.each($(".yen"),function(i,v){
				SH.UTIL.fontSizeAdjust($(v));
			});

			//１つ前の画面に戻る処理
			SH.UTIL.historyBack();

			//firefox対策
			$(".modal").insertAfter("#content");

			//ツールチップ初期化
			SH.UTIL.tooltipInt();

			//IE11用レイアウト調整
			SH.UTIL.ie11LayoutAdjust();

			//第一暗証初期化
			$.each($(".keyboard-alphanumeric"),function(i,v){
				SH.UTIL.keyboardAlphanumeric($(v));
			});

			//第二暗証、ワンタイムパスワード初期化
			$.each($(".keyboard-numeric"),function(i,v){
				SH.UTIL.keyboardNumeric($(v));
			});

			//省略記号
			SH.UTIL.ellipsis();

			//MDLの入力チェック不備対応
			SH.UTIL.IEInputCheck();

			return defer.resolve();
		});
	}
};

SH.UTIL = {

	//ユーザーエージェント情報取得
	checkUA:function(){
		var userAgent = window.navigator.userAgent.toLowerCase();
		if (userAgent.indexOf('edge') != -1) {
			return 'edge';
		} else if (userAgent.indexOf('trident/7') != -1) {
			return 'ie';
		} else if (userAgent.indexOf('chrome') != -1 && userAgent.indexOf('edge') == -1) {
			return 'chrome';
		} else if (userAgent.indexOf('safari') != -1 && userAgent.indexOf('chrome') == -1) {
			return 'safari';
		} else if (userAgent.indexOf('firefox') != -1) {
			return 'firefox';
		} else {
			return false;
		}
	},

	//ヘッダーナビをアクティブ化
	headerNavActive:function(){
		if($("#header-nav").length != 0 && $("body").attr("id").length != 0){
			var category = $("body").attr("id").substring(1,3);
			var categoryArray = new Array("QA","MN","OH","SC");
			for(i in categoryArray){
				if(category == categoryArray[i] && $(".content-title.soudan").length == 0){
					$("#header-nav li").eq(i).addClass("active").children("a").attr("href","javascript:void(0)").removeAttr("onclick");
					break;
				}
			}
		}
	},

	//ヘッダー固定要素切替
	headerFix:function(){
		var _this = this;
		if($(window).scrollTop() >= 64){
			$("#header-fix").addClass("header-fixed");
		}else{
			$("#header-fix").removeClass("header-fixed");
		}
	},

	//トップへ戻るボタン位置固定処理
	toTopShowHide:function(){
		var _this = this;
		var topBtn = $("#to-top");
		if ($(window).scrollTop() > 100) {
			topBtn.removeClass("fix-top");
			scrollHeight = $(document).height();
			scrollPosition = $(window).height() + $(window).scrollTop();
			//スクロールの位置が下部に来た場合
			if (scrollHeight - scrollPosition <= 0) {
				topBtn.addClass("fix-bottom");
			} else {
				//それ以外のスクロールの位置の場合
				topBtn.removeClass("fix-bottom");
			}
		}else{
			topBtn.addClass("fix-top");
		}
	},

	//トップへ戻る処理
	toTop:function(){
		var _this = this;
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	},

	//ズーム関連初期化
	zoomInt:function(){
		//拡大機能利用済みか確認
		if($.cookie("scale") && $.cookie("scale") != "undefined" && $.cookie("scale") != null && $.cookie("scale") != ""){
			$("body").addClass($.cookie("scale"));
			$.each($(".btn__unit--zoom a"),function(i,v){
				if($(v).attr("data-scale") == $.cookie("scale")){
					$(v).addClass("btn-disabled");
				}else{
					$(v).removeClass("btn-disabled");
				}
			});
			SH.UTIL.changeZoom($.cookie("scale"));
		}else{
			$.each($(".btn__unit--zoom a"),function(i,v){
				if($(v).attr("data-scale") == "zoom-medium"){
					$(v).addClass("btn-disabled");
					$.cookie("scale", $(v).attr("data-scale"), {"expires":7,"path":"/"});
				}
			});
			$("body").addClass("zoom-medium");
		};

		//拡大ボタン設定
		$(".btn__unit--zoom a").on("click",function(e){
			var $tgt = $(e.currentTarget);
			SH.UTIL.changeZoom($tgt.attr("data-scale"));
		});

		//リサイズイベントに横スクロール判定登録
		$(window).on("resize",function(e){
			SH.UTIL.hasScrollBar($("body"));
		});

		$(".mdl-tabs__tab").on("click",function(e){
			if($("body").hasClass("firefox zoom-large") || $("body").hasClass("firefox zoom-small")){
				SH.UTIL.changeZoom($.cookie("scale"));
			}
		});
	},

	//ズーム機能
	changeZoom:function(scale){
		var $tgt = $("[data-scale="+scale+"]")
		var $btns = $tgt.siblings();
		var $toTop = $("#to-top").css("transition-duration","0s");

		$.each($btns,function(i,v){
			$(v).removeClass("btn-disabled");
		});

		$tgt.addClass("btn-disabled");

		$("body").removeClass("zoom-small zoom-medium zoom-large").addClass(scale);
		$.cookie("scale", scale, {"expires":7,"path":"/"});

		SH.UTIL.hasScrollBar($("body"));

		if(SH.UA == "firefox"){
			if(scale == "zoom-large" || scale == "zoom-small"){
				SH.UTIL.changeZoomFirefox(400);
			}else{
				$("footer").css("top","");
			}
		}

		$toTop.css("transition-duration","0.3s");
	},

	//ズーム機能（Firefox用）
	changeZoomFirefox:function(timeout){
		if(SH.UA != "firefox"){
			return;
		}
		if(timeout == "undefined" || timeout == null || timeout == ""){
			timeout = 0;
		}
		setTimeout(function(){
			var top = 0;
			top += $("#content").length ? $("#content").height() : 0;
			top += $("header").length ? $("header").height() : 0;
			top += $("h1").length ? $("h1").height() : 0;
			if($("body").hasClass("zoom-small")){
				top += 122*0.92;
			}else if($("body").hasClass("zoom-large")){
				top += 112*1.3;
			}else{
				top += "";
			}
			$("footer").css("top",top);
			$("#shougou").length ? $("#shougou").css("top",top - $("#shougou").height()) : null;
		},timeout);
	},

	//横スクロールバー表示判定
	hasScrollBar : function($tgt) {
		if($tgt.get(0).scrollWidth > $tgt.innerWidth()){
			$("#header-fix").addClass("no-header-fixed");
			$("#to-top").addClass("fix-right");
		}else{
			$("#header-fix").removeClass("no-header-fixed");
			$("#to-top").removeClass("fix-right");
		}
	},

	//セレクト要素初期化
	selectInt:function(){
		//カスタムスクロールバーを設定
		window.setTimeout(function(){//セレクトボックスの生成が終わってから実行したいためタイマーで制御
			$('.mdl-selectfield__list-option-box,.scroll-unit').perfectScrollbar({minScrollbarLength: "12"});
		},500);
	},

	//トグルボタンの初期化
	toggleInt:function(){
		if ($(".mdl-switch").length == 0) {
			return;
		}
		//トグルボタン表示名を設定
		toggleIntInterval = setInterval(function(){
			$.each($(".mdl-switch"),function(i,v){
				if(!$(v).hasClass("is-upgraded")){
					return;
				}
				clearTimeout(toggleIntInterval);
				var label = $(v).find("input").prop("checked") ? "はい" : "いいえ";
				$(v).find(".mdl-switch__thumb").append('<span class="mdl-js-switch-label">' + label + '</span>');
			});
		}, 200);
		//トグルボタン表示名を変更
		$(".mdl-switch input").on("change",function(e){
			var label = $(e.target).prop("checked") ? "はい" : "いいえ";
			$(e.target).closest(".mdl-switch").find(".mdl-js-switch-label").html('<span class="mdl-js-switch-label">' + label + '</span>');
		});
	},

	//スクロールチェックボックス初期化
	scrollCheckboxInt:function(){
		if (SH.UA == "ie" || SH.UA == "firefox") {
			$(".scroll-unit").append("<br/>");
		}
		//スクロールしないとチェックボックスが有効にならない処理
		//初期化
		$.each($(".require-scroll-unit").find("[type=checkbox]"),function(i,v){
			if(!$(v).prop("checked")){
				$(v).prop("disabled",true).closest("label").addClass("is-disabled").removeClass("is-checked");
			}else{
				$(v).closest(".require-scroll-unit").addClass("require-scroll-unit-enabled");//既に非活性解除済みのフラグ
			}
		});

		//スクロール要素とチェックボックスが存在するブロックに.require-scroll-unitを設置
		$(document).on('ps-y-reach-end', function (e) {
			var $scrollUnit = $(e.target);
			var $requireUnit;
			var $checkbox;
			if($scrollUnit.closest(".require-scroll-unit").length > 0){
				$requireUnit = $scrollUnit.closest(".require-scroll-unit");
				if(!$requireUnit.hasClass("require-scroll-unit-enabled")){
					$requireUnit.addClass("require-scroll-unit-enabled");//既に非活性解除済みのフラグ
					$checkbox = $requireUnit.find("[type=checkbox]");//チェックボックスの指定
					$checkbox.prop("disabled",false).closest("label").removeClass("is-disabled");//非活性解除
				}else{
					return false;
				}
			}else{
				return false;
			}
		});
	},

	//アコーディオン初期化
	accordionInt:function(){
		$.each($('.collapse'),function(i,v){
			if(!$(v).hasClass("in")){
				$(v).closest(".accordion").addClass("open");
			}
		});

		$(".accordion>p").on("click",function(e){
			if(!$(this).next().hasClass("collapsing")){
				SH.UTIL.accordion($(e.currentTarget).closest(".accordion"));
			}
		});

		if ($("body").attr("id") != "MUSD1800000") {
			$('.collapse').not("#loginForm").collapse();
		}
	},


	//アコーディオン開閉アイコン処理
	accordion:function($tgt){
		var open = 1;
		if($tgt.hasClass("open")){
			$tgt.removeClass("open");
			var open = -1;
		}else{
			$tgt.addClass("open");
		}
		if($("body").hasClass("firefox zoom-large") || $("body").hasClass("firefox zoom-small")){
			var diff = $tgt.find(".collapse").height()*open;
			$("footer").animate({
				top: "+=" + diff
			}, 350, function() {});
		}
	},

	//お問合せ固定ボタン
	inquryShowHide:function(){
		var $btn = $(".btn__inquiry-fix");
		var $panel = $(".panel--inquiry-fix");
		var $panelLa = $(".panel--inquiry-la-fix");
		if($btn.hasClass("open")){
			$panel.removeClass("open");
			$panelLa.removeClass("open");
			$(".cover-transparent").remove();
			window.setTimeout(function(){
				$btn.removeClass("open");
			},500);
		}else{
			$btn.addClass("open");
			$panel.addClass("open");
			$panelLa.addClass("open");
			var $cover = $("<div class='cover-transparent'></div>");
			$("body").append($cover);
			$cover.on("click",function(e){
				SH.UTIL.inquryShowHide();
			});
		}
	},

	//ボタンパネル内リンク処理
	btnPanelLink:function(){
		$(".link").on("click",function(e){
			e.preventDefault();
			e.stopPropagation();
			var $tgt = $(e.currentTarget);
			if($tgt.attr("target") == "_blank"){
				window.open($tgt.attr("data-href"), '_blank');
			}else{
				window.location.href = $tgt.attr("data-href");
			}
		});
	},

	//必須入力処理初期化
	requireInputInt:function(){
		//非活性ボタンが活性の場合、リンクがアクティブになる処理
		$(".btn-select").on("click",function(e){
			if($(this).hasClass("btn-disabled")){
				e.preventDefault();
			}
		});

		//チェックボックスでのボタン活性制御
		//ボタン活性制御対象となるinput,selectに.require-inputを設置
		//対象となるボタンには.btn-select .btn-disabledを設置
		//GTS0101090にある「うち乗換優遇金額」のラジオボタン制御で
		//選んだ値によって活性制御対象となったりならなかったりする場合、
		//ラジオボタンに.related-inputを追加、その中で関連制御が必要なものに
		//.related-input-true、必要ないものに.related-input-false
		//さらに関連制御が必要なものにプロパティdata-require-targetを追加して
		//値に関連が必要なinputのID名を記載、また関連inputには.require-no-countを設置すること

		$.each($(".require-input"),function(i,v){
			if(!$(v).hasClass("require-no-count")){
				SH.inputNameArr.push($(v).attr("name"));
			}
		});

		SH.inputNameArr = $.unique(SH.inputNameArr);//重複削除

		$(".require-link").on("click",function(e){
			var $tgt = $(e.currentTarget);
			if(!$tgt.hasClass("already-clicked")){
				SH.requireLinkCheckedNum++;
				$tgt.addClass("already-clicked");
				SH.UTIL.requireCheck();
			}
		});

		$(".require-input").on("change keyup",function(e){
			setTimeout(function(){//画面側disable処理のため、若干タイミングをずらす
				SH.UTIL.requireCheck();
			},50);
		});

		//活性非活性処理の初期実行
		SH.UTIL.requireCheck();

	},

	//必須項目入力チェック
	requireCheck:function(){
		var requireNum = SH.inputNameArr.length;
		var checkedNum = 0;

		if(requireNum < 1){
			return false;
		}

		$.each(SH.inputNameArr,function(i,v){
			var $tgt = $("[name=" + v + "]").eq(0);
			var bln = SH.UTIL.inputRequireCheck($tgt);
			if(bln == true || $($tgt[0]).hasClass("require-input-ignore")){
				checkedNum++;
			}
		});

		if(requireNum == checkedNum){
			if(SH.requireLinkNum <= SH.requireLinkCheckedNum){
				$(".btn-select").removeClass("btn-disabled");
				// 固有条件がある場合
				$.each($(".require-unique"),function(i,v){
					var target = $("#" + $(v).attr("data-require-checkbox"));
					if(target.length > 0 && !SH.UTIL.checkboxRequireCheck(target)){
						$(v).addClass("btn-disabled");
					}
					var target2 = $("#" + $(v).attr("data-require-number"));
					if(target2.length > 0 && !SH.UTIL.textRequireCheck(target2)){
						$(v).addClass("btn-disabled");
					}
				});
			}else{
				$(".btn-select").addClass("btn-disabled");
			}
		}else{
			$(".btn-select").addClass("btn-disabled");
		};
	},

	//必須入力項目判定チェック
	inputRequireCheck:function($tgt){
		var type = $tgt.attr("type");
		var bln;

		switch (type){
			case "checkbox":
				bln = SH.UTIL.checkboxRequireCheck($tgt);
				break;
			case "radio":
				bln = SH.UTIL.radioRequireCheck($tgt);
				break;
			case "text":
			case "password":
			case "number":
				bln = SH.UTIL.textRequireCheck($tgt);
				break;
			default:
				bln = SH.UTIL.selectRequireCheck($tgt);
				break;
		}

		return bln;
	},

	//必須チェックボックス入力チェック
	checkboxRequireCheck:function($input){
		var name = $input.attr("name");//同一nameのもののいずれかにチェックが有れば
		var flg = false;
		$.each($("[name=" + name + "]"),function(i,v){
			if($(v).prop("checked")){
				flg = true;
				return false;
			}
		});
		return flg;
	},

	//必須ラジオボタン入力チェック
	radioRequireCheck:function($input){
		var name = $input.attr("name");//同一nameのもののいずれかにチェックが有れば
		var flg = false;
		var relatedFlg = false;
		$.each($("[name=" + name + "]"),function(i,v){
			if($(v).prop("checked")){//チェックがあった場合
				if($(v).hasClass("related-input")){//属性が存在する場合
					if($(v).hasClass("related-input-true")){
						var $tgt = $("#" + $(v).attr("data-require-target"));//連携対象のinputtextを取得
						if($tgt.length > 0){//id(textinputの場合)
							if(SH.UTIL.textRequireCheck($tgt)){
								flg = true;
							}else{
								flg = false;
							}
						}else{//連携対象がradioボタンの場合
							$tgt = $("." + $(v).attr("data-require-target")).eq(0);
							if(SH.UTIL.radioRequireCheck($tgt)){
								flg = true;
							}else{
								flg = false;
							}
						}
					}else{
						flg = true;//"noRelationTrue";
					}
				}else{//属性が存在しない場合
					flg = true;
				}
				return false;
			}
		});

		if(!flg){//もしチェックがOKでない場合、対象のラジオボタンがdisabledになっていないかチェック
			var checkFlg = false;
			$.each($("[name=" + name + "]"),function(i,v){
				if($(v).prop("disabled")){
					checkFlg = true;
					return false;
				}
			});
			if(checkFlg){//もしラジオボタンがdisabledの場合チェックの有無、関連テキストの入力に関係なくtrueを返す
				flg = true;
			}
		}

		return flg;
	},

	//テキストの入力チェック
	textRequireCheck:function($input){
		if($input.val() == ""){//入力値があるかチェック
			return false;
		}else{
			if($input.closest(".mdl-textfield").hasClass("is-invalid")){//バリデートに問題がある場合
				//console.log("問題あり")
				return false;
			}else{
				//console.log("問題なし")
				return true;
			}
		}
	},

	//セレクトの入力チェック
	selectRequireCheck:function($tgt){
		if($tgt.val() == ""){
			$tgt.closest(".mdl-selectfield").addClass("is-invalid");
			return false;
		}else{
			$tgt.closest(".mdl-selectfield").removeClass("is-invalid");
			return true;
		}
	},

	//datePicker初期化
	datePickerInt:function(){
		//日付入力
		$(".date-picker").datepicker({
			dateFormat:"yy年m月d日", //年-月-日(曜日)
			numberOfMonths: 3,
			showButtonPanel: true,
			showAnim:"fadeIn",
			onSelect: function(dateText) {
				if(dateText != ""){
					$(this).closest(".mdl-textfield").addClass("is-dirty");
					SH.UTIL.requireCheck();
				}else{
					$(this).closest(".mdl-textfield").removeClass("is-dirty");
				}
			},
			onClose: function(input, inst) {
				if (inst.id === "option-3" && $('#option-3').attr('name') === "kikanFromGKZ0201010") {
					doCalendarClose();
				}
				if (inst.id === "option-4" && $('#option-4').attr('name') === "kikanToGKZ0201010") {
					doCalendarClose();
				}
			},
			beforeShowDay: function(date) {
				if (date.getDay() == 0) {
					return [true, 'class-sunday', '日曜日'];
				} else if (date.getDay() == 6) {
					return [true, 'class-saturday', '土曜日'];
				} else {
					return [true, 'class-weekday', '平日'];
				}
			},
			beforeShow: function(input, inst){
				inst.dpDiv.css({marginTop: 20 + 'px'});
			},
			currentText:"今月",
			defaultDate:null
		});
	},

	//ウィンドウを閉じる
	windowClose:function(){
		$(".window-close").on("click",function(e){
			if (/Chrome/i.test(navigator.userAgent)) {
				window.close();
			} else {
				window.open('about:blank', '_self').close();
			}
		});
	},

	//入力内容のクリアボタンの初期化
	clearFormInt:function(){
		$(".clear-form").on("click",function(e){
			var target = ($(this).attr('data-target') ? $(this).data("target") : "");
			target = ".clear-form-target" + target;
			SH.UTIL.clearForm(target);
		});
	},

	//入力内容をクリア
	clearForm:function(target){
		var $trg = $(target);
		var top = $trg.offset()["top"] -100;

		$trg.each(function(){
			if($(this).attr("type") == "radio" || $(this).attr("type") == "checkbox"){
				$(this).checked = false;
				$(this).closest("label").removeClass("is-checked");
			}else{
				$(this).val("").parent().removeClass("is-invalid is-dirty");
				$(this).trigger("change");
			}
		});

		if($(window).scrollTop() > top){
			$('body,html').animate({
				scrollTop: top
			}, 500);
		}
  },

	//ログインエリア開閉
	loginAreaOpenClose:function(){
		$("#loginForm").on('show.bs.collapse', function (){
			$("#loginCollapse").addClass("btn--white").removeClass("btn--yellow").find(".mdl-button__label").html("閉じる");
		});
		$("#loginForm").on('shown.bs.collapse', function (){
			$('#input_login_1').focus();
			var $tooltip = $(this).find('.tooltip-info');
			$tooltip.tooltip('show');
			setTimeout(function(){
				$tooltip.tooltip('hide');
			},3000);
		});
		$("#loginForm").on('hide.bs.collapse', function (){
			$("#loginCollapse").addClass("btn--yellow").removeClass("btn--white").find(".mdl-button__label").html("ログイン");
		});
	},

	//フォントサイズ調整
	fontSizeAdjust:function($tgt){
		if ($tgt.is(":hidden")) {
			return;
		}
		var zoomValue = 0.6;
		if ($("body").hasClass("zoom-small")) {
			zoomValue = 0.53;//暫定値
		} else if ($("body").hasClass("zoom-large")) {
			zoomValue = 0.82;//暫定値
		}
		var $tgtWidth = $tgt.width()-(20+16);//padding分と「円」分を引く
		if ($tgt.hasClass("content-clear")) {
			$tgtWidth += 16;//「円」分
		}
		var maxFontSize = 24;
		var minFontSize = 12;
		var dashNum = SH.UTIL.strCount($tgt.text(),",");
		var contentNum = $tgt.hasClass("plus") ? 1 : 0;
		var textNum = $tgt.text().length-(dashNum/2)+contentNum;
		var sizeArr = [];
		var fontSizeArr = [];
		for(var i = maxFontSize; i>=minFontSize; i--){
			sizeArr.push(textNum*(i*zoomValue));
			fontSizeArr.push(i+"px");
		}

		$.each(sizeArr,function(i,v){
			if(v > $tgtWidth){
				if(i+1 >= sizeArr.length ){
					if($tgt.css("font-size") != fontSizeArr[i]){
						$tgt.css('font-size', fontSizeArr[i]).removeClass("fs24");
					}
					return false;
				}else{
					return true;
				}
			}else{
				if($tgt.css("font-size") != fontSizeArr[i]){
					$tgt.css('font-size', fontSizeArr[i]).removeClass("fs24");
				}
				return false;
			}
		});
	},

	//特定文字のカウント
	strCount:function(all, part) {
		return (all.match(new RegExp(part, "g")) || []).length;
	},

	//一つ前の画面へ戻る処理
	historyBack:function(){
		$(".btn-history-back").on("click",function(e){
			window.history.back(-1);
			return false;
		});
	},

	//ツールチップ初期化
	tooltipInt:function(){
		//ヘルプツールチップ初期化
		SH.UTIL.helpTooltip('[data-toggle="help"]');

		//その他のツールチップ
		$('.tooltip-info').tooltip({
			template: '<div class="tooltip" role="tooltip"><div class="tooltip-inner"></div></div>'
		});

		$('.tooltip-alert').tooltip({
			template: '<div class="tooltip tooltip-alert" role="tooltip"><div class="tooltip-inner"></div></div>',
			trigger: "manual"
		}).tooltip('show');

		$('.tooltip-alert-side-nav').tooltip({
			template: '<div class="tooltip tooltip-alert tooltip-alert-side-nav" role="tooltip"><div class="tooltip-inner"></div></div>',
			trigger: "manual"
		}).tooltip('show');
	},

	//ヘルプツールチップ初期化
	helpTooltip:function(tgt){

		$.each($(tgt),function(i,v){
			var $tgt = $(v);
			var direc = $tgt.attr("data-placement");
			var html = "";
			var className = ($tgt.attr("data-class") ? $tgt.attr("data-class") : "");

			html += '<div class="help-tooltip ' + direc + " " + className + '" role="tooltip">';
			html += 	'<div class="help-tooltip-inner">';
			html += 		$tgt.attr("data-content");
			html += 	'</div>';
			html += '</div>';

			var $html = $(html);
			$tgt.append($html);

			var width = $tgt.attr("data-width");
			if(width != "undefined" && width != null && width != ""){
				$html.css("min-width",width+"px");
				$html.css("max-width",width+"px");
			}

			$tgt.on("mouseover",function(e){
				e.preventDefault();
				e.stopPropagation();
				$tgt.addClass("show");
					if(direc == "right" || direc == "left"){
						var top = (Math.floor($html.height()/2)*-1)+12;
						if($("body").hasClass("firefox")&&($("body").hasClass("zoom-large"))){
							top = top+12;
						}
						$html.css("top",top+"px");
					}else{
						var left = (Math.floor($html.width()/2)*-1)+27;
						if(direc == "bottom-right"){
							left = (Math.floor($html.width()/2)*-1)+196;
						}
						if(($("body").hasClass("firefox") && !$html.closest("#header-fix").length) || $("body").hasClass("ie")){
							if($("body").hasClass("zoom-large")){
								left = left +75;
							}else if($("body").hasClass("zoom-small")){
								left = left -20;
							}
						}
						var width = $tgt.attr("data-width");
						if(width != "undefined" && width != null && width != ""){
							left -= 7;
							if(($("body").hasClass("firefox") && !$html.closest("#header-fix").length) || $("body").hasClass("ie")){
								if($("body").hasClass("zoom-large")){
									left -= 26;
								}else if($("body").hasClass("zoom-small")){
									left += 5;
								}
							}
						}
						$html.css("left",left+"px");
					}
			});

			$html.on("click mousedown dblclick mouseup",function(e){
				return false;
			});

			$tgt.find("a").on("click",function(e){
				var $a = $(this);
				if($a.attr("target") != "_blank"){
					window.location.href = $a.attr("href");
				}else{
					window.open($a.attr("href"));
				}
			});

			$html.on("mouseover",function(e){
				$html.addClass("show");
			});
			$html.on("mouseleave",function(e){
				$html.removeClass("show");
			});

			$tgt.on("mouseleave",function(e){
				setTimeout(function(){
					if(!$html.hasClass("show")){
						$tgt.removeClass("show");
					}
				},300);
			});

		});
	},

	//IE11用レイアウト調整
	ie11LayoutAdjust:function(){
		if(SH.UA == "ie"){
			$(window).on("resize",function(e){
				//拡大時
				if($("body").hasClass("zoom-large")){
					var w = Math.floor($(window).width()/1.3);
					if(w < 1260){
						w = 1260;
					}
					$("#wrapper").width(w);
				}
			});
		};
	},

	//第一暗証(フルキーボード)
	keyboardAlphanumeric : function($tgt){
		var $input = $tgt.find("input");
		var title = $tgt.attr("data-keyboard-title");
		var panel = '<div class="panel">';
		panel +=    '<h3 class="title__l title__l--red">'+ title +'</h3>';
		panel +=    '<a href="javascript:void(0);" class="btn__close"></a>';
		panel +=    '<p class="ml30 mt20"><span class="fc-light-red fwb">英文字は大文字・小文字を区別しますのでご注意ください。<BR>入力完了後、×ボタンを押してください。</span></p>';
		panel +=    '<div class="key-area assist-no-show">';
		panel +=    '	<div>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="1">1</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="2">2</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="3">3</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="4">4</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="5">5</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="6">6</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="7">7</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="8">8</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="9">9</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="0">0</a>';
		panel +=    '	</div>';
		panel +=    '	<div>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="a">a</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="b">b</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="c">c</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="d">d</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="e">e</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="f">f</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="g">g</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="h">h</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="i">i</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="j">j</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="k">k</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="l">l</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="m">m</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="n">n</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="o">o</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="p">p</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="q">q</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="r">r</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="s">s</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="t">t</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="u">u</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="v">v</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="w">w</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="x">x</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="y">y</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="z">z</a>';
		panel +=    '	</div>';
		panel +=    '	<div>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="A">A</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="B">B</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="C">C</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="D">D</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="E">E</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="F">F</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="G">G</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="H">H</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="I">I</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="J">J</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="K">K</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="L">L</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="M">M</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="N">N</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="O">O</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="P">P</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="Q">Q</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="R">R</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="S">S</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="T">T</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="U">U</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="V">V</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="W">W</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="X">X</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="Y">Y</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="Z">Z</a>';
		panel +=    '	</div>';
		panel +=    '</div>';
		panel +=    '<div class="panel-inner-bottom-gray flex-center">';
		panel +=    '	<a href="javascript:void(0)" role="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--l btn--white delete w140">';
		panel +=    '		<span class="mdl-button__label">１文字削除</span>';
		panel +=    '	</a>';
		panel +=    '	<a href="javascript:void(0)" role="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--l btn--white delete-all w140">';
		panel +=    '		<span class="mdl-button__label">全て削除</span>';
		panel +=    '	</a>';
		panel +=    '</div>';
		panel +=    '</div>';
		var $panel = $(panel);
		$panel.appendTo($tgt).hide();
		//キーボードの閉じるボタン
		SH.UTIL.keyboardClose($panel,$tgt,$input);
		//キーボード表示・非表示
		SH.UTIL.keyboardShowHide($panel,$tgt,$input);
		//キーボードの入力
		SH.UTIL.keyboardInput($panel,$tgt,$input);
		//キーボードの一文字削除
		SH.UTIL.keyboardDeleteOneChara($panel,$tgt,$input);
		//キーボードの全入力削除
		SH.UTIL.keyboardDeleteAllChara($panel,$tgt,$input);
		//inputへイベントを伝播
		$panel.on("click",function(e){
			$input.trigger("change");
		});
	},

	//第二暗証、ワンタイムパスワード初期化
	keyboardNumeric : function($tgt){
		var $input = $tgt.find("input");
		var title = $tgt.attr("data-keyboard-title");
		var panel = '<div class="panel">';
		panel +=    '	<h3 class="title__l title__l--red">'+ title +'</h3>';
		panel +=    '	<a href="javascript:void(0);" class="btn__close"></a>';
		panel +=    '	<p class="ml30 mt20"><span class="fc-light-red fwb">入力完了後、×ボタンを押してください。</span></p>';
		panel +=    '	<div class="key-area assist-no-show">';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="1">1</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="2">2</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="3">3</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="4">4</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="5">5</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="6">6</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="7">7</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="8">8</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="9">9</a>';
		panel +=    '		<a href="javascript:void(0);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--white assist-no-show" data-num="0">0</a>';
		panel +=    '	</div>';
		panel +=    '	<div class="panel-inner-bottom-gray flex-equal">';
		panel +=    '		<a href="javascript:void(0)" role="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--l btn--white delete w140">';
		panel +=    '			<span class="mdl-button__label">１文字削除</span>';
		panel +=    '		</a>';
		panel +=    '		<a href="javascript:void(0)" role="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent btn--l btn--white delete-all w140">';
		panel +=    '			<span class="mdl-button__label">全て削除</span>';
		panel +=    '		</a>';
		panel +=    '	</div>';
		panel +=    '</div>';
		var $panel = $(panel);
		$panel.appendTo($tgt).hide();
		//キーボードの閉じるボタン
		SH.UTIL.keyboardClose($panel,$tgt,$input);
		//キーボード表示・非表示
		SH.UTIL.keyboardShowHide($panel,$tgt,$input);
		//キーボードの入力
		SH.UTIL.keyboardInput($panel,$tgt,$input);
		//キーボードの一文字削除
		SH.UTIL.keyboardDeleteOneChara($panel,$tgt,$input);
		//キーボードの全入力削除
		SH.UTIL.keyboardDeleteAllChara($panel,$tgt,$input);
		//inputへイベントを伝播
		$panel.on("click",function(e){
			$input.trigger("change");
		});
	},

	//キーボードの閉じるボタン
	keyboardClose:function($panel,$tgt,$input){
		$panel.find(".btn__close").on("click",function(e){
			$panel.hide();
			$tgt.removeClass("is-focused");
			if($input.val().length == 0){
				$tgt.removeClass("is-dirty");
				$tgt.removeClass("is-invalid");
			}
		});
	},

	//キーボードの表示・非表示
	keyboardShowHide:function($panel,$tgt,$input){
		$tgt.find(".keyboard-switch").on("click",function(e){
			if($panel.is(':visible')){
				$panel.hide();
				$tgt.removeClass("is-focused");
				if($input.val().length == 0){
					$tgt.removeClass("is-dirty");
					$tgt.removeClass("is-invalid");
				}
			}else{
				$panel.show();
				$tgt.addClass("is-focused");
			}
		});
	},

	//キーボードの入力
	keyboardInput:function($panel,$tgt,$input){
		$panel.find(".key-area").find("a").on("click",function(e){
			var selectNum = $(e.currentTarget).attr("data-num");
			var inputValue = $input.val();
			if(inputValue.length < $input.attr("maxlength")){
				$input.val($input.val() + selectNum);
				SH.UTIL.keyboardInputCheck($input,$tgt);
				$tgt.addClass("is-dirty");
			}
		});
	},

	//キーボードの入力チェック
	keyboardInputCheck:function($input,$tgt){
		SH.UTIL.triggerEvent($input.get(0),"input");
		setTimeout(function(){
				if($tgt.hasClass("is-invalid")){
					return false;
				}else{
					$tgt.removeClass("is-invalid");
				}
			if($input.val().length == 0){
				$tgt.removeClass("is-dirty");
				$tgt.removeClass("is-invalid");
			}
		},50);
	},

	//オリジナルトリガーイベント生成
	triggerEvent:function(element, event) {
		if (document.createEvent) {
			// IE以外
			var evt = document.createEvent("HTMLEvents");
			evt.initEvent(event, true, true ); // event type, bubbling, cancelable
			return element.dispatchEvent(evt);
		} else {
			// IE
			var evt = document.createEventObject();
			return element.fireEvent("on"+event, evt)
		}
	},

	//キーボードの一文字削除
	keyboardDeleteOneChara:function($panel,$tgt,$input){
		$panel.find(".delete").on("click",function(e){
			var inputValue = $input.val();
			if(inputValue.length > 0){
				$input.val(inputValue.substr(0, inputValue.length-1));
				SH.UTIL.keyboardInputCheck($input,$tgt);
			}
		});
	},

	//キーボードの全入力削除
	keyboardDeleteAllChara:function($panel,$tgt,$input){
		$panel.find(".delete-all").on("click",function(e){
			$input.val("");
			$tgt.removeClass("is-dirty");
			$tgt.removeClass("is-invalid");
		});
	},

	ellipsis:function(){
		var $trg = $(".ellipsis");
		$trg.each(function(){
			var height = $(this)[0].scrollHeight;
			var maxHeight = $(this).height()+5;
			if(maxHeight < height){
				var str = $(this).text();
				var i = (str.length > 200 ? 200 : (str.length-1));
				for(i;i>0;i--){
					str = str.substr(0, i);
					var newStr = str + "…";
					$(this).text(newStr);
					height = $(this)[0].scrollHeight;
					if(maxHeight >= height){
						break;
					}
				}
			}
		});
	},

	//MDLの入力チェック不備対応（IEで表示時に日本語だとエラーにならない）
	IEInputCheck:function(){
		if(SH.UA == "ie"){
			var $mdlInputs = $(".mdl-textfield__input");
			for (var i = 0, l = $mdlInputs.length; i < l; i++) {
				var str = $mdlInputs[i].value;
				var patt = $mdlInputs[i].getAttribute("pattern");
				if(str != "" && patt != "undefined" && patt != null && patt != ""){
					patt = (patt.slice(-1) != "$" ? patt + "$": patt);
					patt = (patt.charAt(0) != "^" ? "^" + patt : patt);
					if(!RegExp(patt).test(str)){
						$($mdlInputs[i]).closest(".mdl-textfield").addClass("is-invalid");
					}
				}
			}
		}
	}
};

//初期化実行
$(function(){new SH();});
