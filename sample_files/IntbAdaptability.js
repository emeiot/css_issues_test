Main = function(){

  var countFlg = false;
  if($("#adaptCheckAns10_3").prop("checked") && $("#adaptCheckAns11_3").prop("checked")){
      $("#information-gathering").find("input").prop("disabled",true).prop("checked",false);
      $("#information-gathering").find("label").addClass("is-disabled").removeClass("is-checked");
      countFlg = true;
    }else{
      $("#information-gathering").find("label").removeClass("is-disabled");
      $("#information-gathering").find("input").prop("disabled",false);
      countFlg = false;
    };

  $("#information-frequency").find("input").on("change",function(e){
    if($("#adaptCheckAns10_3").prop("checked") && $("#adaptCheckAns11_3").prop("checked")){
      $("#information-gathering").find("input").prop("disabled",true).prop("checked",false);
      $("#information-gathering").find("label").addClass("is-disabled").removeClass("is-checked");
      countFlg = true;
    }else{
      $("#information-gathering").find("label").removeClass("is-disabled");
      $("#information-gathering").find("input").prop("disabled",false);
      countFlg = false;
    }
  });


  $("#investment-experience").find("input").on("change",function(e){
    $.each($("#no-exp-radio").find("input"),function(i,v){
      var $tgt = $(v);
      var index = $("#no-exp-radio").find("input").index($tgt);
      var $etgt = $("#no-exp-radio-target").find("input").eq(index);
      var $exp1 = $("#exp-radio-1").find("input").eq(index);
      var $exp2 = $("#exp-radio-2").find("input").eq(index);
      var $exp3 = $("#exp-radio-3").find("input").eq(index);
      if($tgt.prop("checked") || !($exp1.prop("checked") || $exp2.prop("checked") || $exp3.prop("checked"))){
        $etgt.prop("disabled",true).prop("checked",false);
        $etgt.closest("label").addClass("is-disabled").removeClass("is-checked");
      }else{
        $etgt.prop("disabled",false);
        $etgt.closest("label").removeClass("is-disabled");
      }
    });
  });

  $("#investment-experience").find("input").eq(0).trigger("change");

  //項目内のinputに変化があったら次の項目を開く　1,2,7,8
  $(".simple-select").find("input").on("change",function(e){
    nextBlockShow($(e.target));
  });

  //3,4,6
  $(".multi-select").find("input").on("change",function(e){
    var radioNameArr = [];
    var checkboxNameArr = [];
    var radioCount = 0;
    var checkboxCount = 0;

    //nameの洗い出し
    $.each($(this).closest(".accordion").find("input"),function(i,v){
      if($(v).attr("type") == "radio"){
        radioNameArr.push($(v).attr("name"));
      }else{
        checkboxNameArr.push($(v).attr("name"));
      }
    });

    //nameのカブり削除
    radioNameArr = radioNameArr.filter(function (x, i, self) {
        return self.indexOf(x) === i;
    });

    //チェックが入っていたらカウントアップ
    $.each(radioNameArr,function(i,v){
      $.each($("[name="+ v +"]"),function(ia,va){
        if($(va).prop("checked")){
          radioCount++;
          if($(va).hasClass("no-check-info")){
            checkboxCount += 0.5;
          }
        }
      });
    });
    $.each(checkboxNameArr,function(i,v){
      $.each($("[name="+ v +"]"),function(ia,va){
        if($(va).prop("checked")){
          checkboxCount++;
        }
      });
    });

    //name数とcount数が一致（すべての選択項目が何某か入力があったら）
    if(radioCount >= radioNameArr.length && (checkboxCount >= 1 || checkboxNameArr.length == 0)){
      nextBlockShow($(e.target));
    }
  });

  //次のブロックを開く
  var nextBlockShow = function($tgt){
    var $nextBlock = $tgt.closest(".accordion").next();
    $nextBlock.addClass("open");
    $nextBlock.children("div").collapse("show");

    if($nextBlock.hasClass("no-select")){
      $nextBlock.next().addClass("open");
      $nextBlock.next().children("div").collapse("show");
    }

    SH.UTIL.changeZoomFirefox(400);
  }

  //すべて入力済みの場合、次へボタンをアクティブ(5.は任意のため除く)
  $("input").on("change",function(e){
    var nameArr = [];
    var count = 0;
    var count_co3 = 0;
    var count_co4 = 0;

    $.each($(".accordion"),function(i,v){
      if(i == 4){
        return true;
      }else{
        $.each($(v).find("input"),function(ia,va){
          if($(va).attr("type")!="checkbox"){
            nameArr.push($(va).attr("name"));
          }
        });
      }
    });

    //nameのカブり削除
    nameArr = nameArr.filter(function (x, i, self) {
        return self.indexOf(x) === i;
    });

    //チェックが入っていたらカウントアップ
    $.each(nameArr,function(i,v){
      $.each($("[name="+ v +"]"),function(ia,va){
        if($(va).prop("checked")){
          count += 1;
          return false;
        }
      });
    });

    $("#collapse-3 input").each(function(){
      if($(this).attr("type")=="checkbox"){
        if($(this).prop("checked")){
          count_co3++;
        }
      }
    });
    $("#collapse-4 input").each(function(){
      if($(this).attr("type")=="checkbox"){
        if($(this).prop("checked")){
          count_co4++;
        }
      }
    });
    if(count_co3 > 0 && (count_co4 > 0 || countFlg)){
      count++;
    }

    //必須name数とcount数が一致（すべての選択項目が何某か入力があったら）
    if(count > nameArr.length){
      $(".btn__next").removeClass("btn-disabled");
    }else{
      $(".btn__next").addClass("btn-disabled");
    }
  });

  $("input[type=hidden]").eq(0).trigger("change");
};
